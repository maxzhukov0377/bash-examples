#!/bin/bash
FireFox_min_version="89.0.1.0"
Chrome_min_version="100.0.4472.101"
Adobe_min_version="21.005.20048"
JDK_min_version="8.0.2610.12"
MSOffice_min_version="166.0.14026.20270"
Opera_min_version="77.0.4054.90"
Pusle_min_version="9.1.9"
OpenVPN_min_version="3.1.0"
Openconnect_min_version="1.5.3.0"
Forti_client_min_version="6.4.2"
Webex_min_version="41.6.1.19162"
Skype_min_version="8.73"
SkypeFB_min_version="16.29.57"
WinZ_min_version="9.0.5520"
Lib_office_min_version="7.1.4.2"
OpenOffice_min_version="4.1.10"


Compare-AppVersions () {
a=$1
b=$2
InstalledVersion=$(echo $a | tr "." " ")
ReferenceVersion=$(echo $b | tr "." " ")
#Installed app major version
Major_I="$(echo $InstalledVersion | awk '{print $1}')"
#Installed app middle version
Middle_I="$(echo $InstalledVersion | awk '{print $2}')"
#Installed app minor version
Minor_I="$(echo $InstalledVersion | awk '{print $3}')"
#installed app revision
Revision_I="$(echo $InstalledVersion | awk '{print $4}')"
#installed app build
Build_I="$(echo $InstalledVersion | awk '{print $5}')"
#Reference app major version
Major_R="$(echo $ReferenceVersion | awk '{print $1}')"
#Reference app middle version
Middle_R="$(echo $ReferenceVersion | awk '{print $2}')"
#Reference app minor version
Minor_R="$(echo $ReferenceVersion | awk '{print $3}')"
#Reference app revision
Revision_R="$(echo $ReferenceVersion | awk '{print $4}')"
#Reference app build
Build_R="$(echo $ReferenceVersion | awk '{print $5}')"
#Compare version
if [ "$a" == "$b" ]
then echo 0
elif [ $Major_I -lt $Major_R  ]
then echo 0
elif [ $Major_I -gt $Major_R ]
then echo 1
elif [[ -n $Middle_I && $Middle_I -lt $Middle_R ]]  
then echo 0
elif [[ -n $Minor_I && $Minor_I -lt $Minor_R ]] 
then echo 0
elif [[ -n $Revision_I && $Revision_I -lt $Revision_R ]] 
then echo 0
elif [[ -n $Build_I && $Build_I -lt $Build_R ]] 
then echo 0
else echo 1
fi
}
vulnerableApps=()
#FireFox comparing version
if [[ -f /Applications/Firefox.app/Contents/MacOS/firefox ]]
then
FireFox_installed_version=$(/Applications/Firefox.app/Contents/MacOS/firefox -version | awk '{print $3}')
FireFox_comparing=$(Compare-AppVersions $FireFox_min_version $FireFox_installed_version)
else
FireFox_comparing=0
fi
#Chromme commparing version
if [[ -f /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome ]]
then
Chrome_installed_version=$(/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome -version | awk '{print $3}')
Chrome_comparing=$(Compare-AppVersions $Chrome_min_version $Chrome_installed_version)
else
Chrome_comparing=0
fi
#JDK comparing version
if [[ -f /usr/bin/java  ]]
then
JDK_installed_version=$(java --version | tail -n1 | egrep -o '[0-9]+\.[0-9]+\.[0-9]')
elif [[ $? -eq 0 ]]
then
JDK_comparing=$(Compare-AppVersions $JDK_min_version $JDK_installed_version)
else
JDK_comparing=0
fi
#Adobe comparing version
if [[ -f /Applications/Adobe\ Acrobat\ Reader\ DC.app/Contents/Info.plist ]]
then
Adobe_installed_version=$(defaults read /Applications/Adobe\ Acrobat\ Reader\ DC.app/Contents/Info.plist CFBundleShortVersionString)
Adobe_comparing=$(Compare-AppVersions $Adobe_min_version $Adobe_installed_version)
else
Adobe_comparing=0
fi
#MS office comparing version
if [[ -f /Applications/Microsoft\ Outlook.app/Contents/Info.plist ]]
then
MSOffice_installed_version=$(defaults read /Applications/Microsoft\ Outlook.app/Contents/Info.plist CFBundleShortVersionString)
MS_office_comparing=$(Compare-AppVersions $MSOffice_min_version $MSOffice_installed_version)
else
MS_office_comparing=0
fi
#Opera comparing version
if [[ -f /Applications/Opera.app/Contents/Info.plist ]]
then
Opera_installed_version=$(defaults read /Applications/Opera.app/Contents/Info.plist CFBundleVersion)
Opera_comparing=$(Compare-AppVersions $Opera_min_version $Opera_installed_version)
else
Opera_comparing=0
fi
#Pulse Secure comparing
if [[ -f /Applications/Pulse\ Secure.app/Contents/Info.plist ]]
then
Pulse_installed_version=$(defaults read /Applications/Pulse\ Secure.app/Contents/Info.plist CFBundleShortVersionString)
Pulse_comparing=$(Compare-AppVersions $Pusle_min_version $Pulse_installed_version)
else
Pulse_comparing=0
fi
#OpenVPN comparing
if [[ -f /Applications/OpenVPN\ Connect/OpenVPN\ Connect.app/Contents/Info.plist ]]
then
OpenVPN_installed_version=$(defaults read /Applications/OpenVPN\ Connect/OpenVPN\ Connect.app/Contents/Info.plist CFBundleShortVersionString)
OpenVPN_comparing=$(Compare-AppVersions $OpenVPN_min_version $OpenVPN_installed_version)
else
OpenVPN_comparing=0
fi
#Openconnect comparing
if [[ -f /usr/local/bin/openconnect ]]
then
Openconnect_installed_version=$(openconnect --version | grep version | awk '{ print $3}' | cut -c 2-)
Openconnect_comparing=$(Compare-AppVersions $Openconnect_min_version $Openconnect_installed_version)
else
Openconnect_comparing=0
fi
#FortiClient comparing
if [[ -f /Applications/FortiClient.app/Contents/Info.plist ]]
then
Forti_client_installed_version=$(defaults read /Applications/FortiClient.app/Contents/Info.plist CFBundleShortVersionString)
Forti_client_comparing=$(Compare-AppVersions $Forti_client_min_version $Forti_client_installed_version)
else
Forti_client_comparing=0
fi
#Webex comparing
if [[ -f /Applications/Webex.app/Contents/Info.plist ]]
then
Webex_installed_version=$(defaults read /Applications/Webex.app/Contents/Info.plist CFBundleShortVersionString)
Webex_comparing=$(Compare-AppVersions $Webex_min_version $Webex_installed_version)
else
Webex_comparing=0
fi
#Skype comparing
if [[ -f /Applications/Skype.app/Contents/Info.plist ]]
then
Skype_installed_version=$(defaults read /Applications/Skype.app/Contents/Info.plist CFBundleShortVersionString)
Skype_comparing=$(Compare-AppVersions $Skype_min_version $Skype_installed_version)
else
Skype_comparing=0
fi
#Skype for business comparing
if [[ -f /Applications/Skype\ for\ Business.app/Contents/Info.plist ]]
then
SkypeFB_installed_version=$(defaults read /Applications/Skype\ for\ Business.app/Contents/Info.plist CFBundleShortVersionString)
SkypeFB_comparing=$(Compare-AppVersions $SkypeFB_min_version $SkypeFB_installed_version)
else
SkypeFB_comparing=0
fi
#Win zip comparing
if [[ -f /Applications/WinZip.app/Contents/Info.plist ]]
then
WinZ_installed_version=$(defaults read /Applications/WinZip.app/Contents/Info.plist CFBundleShortVersionString)
Winz_comparing=$(Compare-AppVersions $WinZ_min_version $WinZ_installed_version)
else
Winz_comparing=0
fi
#Libre Office comparing
if [[ -f /Applications/LibreOffice.app/Contents/Info.plist ]]
then
Lib_office_installed_version=$(defaults read /Applications/LibreOffice.app/Contents/Info.plist CFBundleShortVersionString)
Lib_office_comparing=$(Compare-AppVersions $Lib_office_min_version $Lib_office_installed_version)
else
Lib_office_comparing=0
fi
#OpenOffice comparing
if [[ -f /Applications/OpenOffice.app/Contents/Info.plist ]]
then
OpenOffice_installed_version=$(defaults read /Applications/OpenOffice.app/Contents/Info.plist CFBundleShortVersionString)
OpenOffice_comparing=$(Compare-AppVersions $OpenOffice_min_version $OpenOffice_installed_version)
else
OpenOffice_comparing=0
fi



#Creating a list with vulnerable apps
if [ "$FireFox_comparing" -eq 1 ]
then vulnerableApps+=Firefox._Your_version_is_$FireFox_installed_version._Please_update_it_to_at_least_$FireFox_min_version,
fi
if [ "$Chrome_comparing" -eq 1 ]
then vulnerableApps+=_Google_Chrome._Your_version_is_$Chrome_installed_version._Please_update_it_to_at_least_$Chrome_min_version, 
fi
if [[  "$JDK_comparing" -eq 1 ]]
then vulnerableApps+=_JDK._Your_version_is_$JDK_installed_version._Please_update_it_to_at_least_$JDK_min_version, 
fi
if [[ "$Adobe_comparing" -eq 1 ]]
then vulnerableApps+=_Adobe_Reader._Your_version_is_$Adobe_installed_version._Please_update_it_to_at_least_$Adobe_min_version, 
fi
if [ "$MS_office_comparing" -eq 1 ]
then vulnerableApps+=_MS_office._Your_version_is_$MSOffice_installed_version._Please_update_it_to_at_least_$MSOffice_min_version, 
fi
if [ "$Opera_comparing" -eq 1 ]
then vulnerableApps+=_Opera._Your_version_is_$Opera_installed_version._Please_update_it_to_at_least_$Opera_min_version, 
fi
if [ "$Pulse_comparing" -eq 1 ]
then vulnerableApps+=_Pulse_Secure._Your_version_is_$Pulse_installed_version._Please_update_it_to_at_least_$Pusle_min_version, 
fi
if [ "$OpenVPN_comparing" -eq 1 ]
then vulnerableApps+=_Open_VPN._Your_version_is_$OpenVPN_installed_version._Please_update_it_to_at_least_$OpenVPN_min_version, 
fi
if [ "$Openconnect_comparing" -eq 1 ]
then vulnerableApps+=_Openconnect._Your_version_is_$Openconnect_installed_version._Please_update_it_to_at_least_$Openconnect_min_version, 
fi
if [ "$Forti_client_comparing" -eq 1 ]
then vulnerableApps+=_FortiClient._Your_version_is_$Forti_client_installed_version._Please_update_it_to_at_least_$Forti_client_min_version, 
fi
if [ "$Webex_comparing" -eq 1 ]
then vulnerableApps+=_Webex._Your_version_is_$Webex_installed_version._Please_update_it_to_at_least_$Webex_min_version, 
fi
if [ "$Skype_comparing" -eq 1 ]
then vulnerableApps+=_Skype._Your_version_is_$Skype_installed_version._Please_update_it_to_at_least_$Skype_min_version, 
fi
if [ "$SkypeFB_comparing" -eq 1 ]
then vulnerableApps+=_Skype_for_business._Your_version_is_$SkypeFB_installed_version._Please_update_it_to_at_least_$SkypeFB_min_version, 
fi
if [ "$Winz_comparing" -eq 1 ]
then vulnerableApps+=_WinZip._Your_version_is_$WinZ_installed_version._Please_update_it_to_at_least_$WinZ_min_version, 
fi
if [ "$Lib_office_comparing" -eq 1 ]
then vulnerableApps+=_LibreOffice._Your_version_is_$Lib_office_installed_version._Please_update_it_to_at_least_$Lib_office_min_version, 
fi
if [ "$OpenOffice_comparing" -eq 1 ]
then vulnerableApps+=_OpenOffice._Your_version_is_$OpenOffice_installed_version._Please_update_it_to_at_least_$OpenOffice_min_version, 
fi

#Getting final result
Total_comparing=$(($FireFox_comparing+$Chrome_comparing+$JDK_comparing+$Adobe_comparing+$MS_office_comparing+$Opera_comparing+$Pulse_comparing+$OpenVPN_comparing+$Openconnect_comparing+$Forti_client_comparing+$Webex_comparing+$Skype_comparing+$SkypeFB_comparing+$Lib_office_comparing+OpenOffice_comparing))
if [ $Total_comparing -eq 0 ]
then echo 0
else echo $vulnerableApps | tr "_" " " | tr "," "\n"
fi