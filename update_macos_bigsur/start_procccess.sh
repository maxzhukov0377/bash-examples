#!/bin/bash
min_ver="11.5.3"
inst_ver=$(sw_vers -productVersion)
func() {
min_ver=$1
inst_ver=$2
InstalledVersion=$(echo $min_ver | tr "." " ")
ReferenceVersion=$(echo $inst_ver | tr "." " ")
#Installed app major version
Major_I="$(echo $InstalledVersion | awk '{print $1}')"
#Installed app middle version
Middle_I="$(echo $InstalledVersion | awk '{print $2}')"
#Installed app minor version
Minor_I="$(echo $InstalledVersion | awk '{print $3}')"
#Reference app major version
Major_R="$(echo $ReferenceVersion | awk '{print $1}')"
#Reference app middle version
Middle_R="$(echo $ReferenceVersion | awk '{print $2}')"
#Reference app minor version
Minor_R="$(echo $ReferenceVersion | awk '{print $3}')"
#Compare version
if [ "$min_ver" == "$inst_ver" ]
then echo 0
elif [ $Major_I -lt $Major_R  ]
then echo 0
elif [ $Major_I -gt $Major_R ]
then echo 1
elif [[ -n $Middle_I && $Middle_I -lt $Middle_R ]]  
then echo 0
elif [[ -n $Minor_I && $Minor_I -lt $Minor_R ]] 
then echo 0
else echo 1
fi
}

if [[ $inst_ver == *"11"* ]]
then
compare=$(func $min_ver $inst_ver)
else
compare=0
fi

if [[ $compare != 0 ]]; then 
/usr/local/bin/hubcli notify -t "Update to macOS Big Sur 11.6"  -i "Please update your macOS to 11.6 - it takes about 1 hour. Click Start to proceed. Your device will be restarted." -a "Start" -b "softwareupdate --force --background -i -r -R" -c "Postpone for 2 hours"
fi
