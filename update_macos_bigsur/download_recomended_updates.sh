#!/bin/bash
check_capacity=$(df -h | sed -n -e 2p | awk '{print $4}' | rev | cut -c 3- | rev)
min_capacity="12" #change the minimum value in GB
process_check=$(ps aux | grep "/usr/sbin/softwareupdate -d -r" | grep -v -q  "grep /usr/sbin/softwareupdate -d -r" ; echo $?)
echo `date '+%Y/%m/%dT%H:%M:%S'`"=> Profile download Update macOS => Assigned..." >> /var/log/ws1_download_log.txt

start_download(){
if [[ $process_check -eq 1 ]]
then
/usr/sbin/softwareupdate -d -r >>/var/log/ws1_download_log.txt && echo `date '+%Y/%m/%dT%H:%M:%S'`"=> Profile download Update macOS => Starting..." >> /var/log/ws1_download_log.txt
else
exit
fi
}

if (( $(echo "$min_capacity > $check_capacity" | bc -l) ));
then 
	/usr/local/bin/hubcli notify -t "Update to macOS Big Sur 11.6" -i "There is not enough space on your device to download the update. Minimum 12GB is required. Click Open to go to storage Management" -a "Open" -b 'open -a "storage Management"' && echo `date '+%Y/%m/%dT%H:%M:%S'` "=> Profile download Update macOS => Found updates, insufficient free space..." >> /var/log/ws1_download_log.txt
else
start_download
fi
